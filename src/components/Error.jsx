import { Flex, Heading } from "@chakra-ui/react";
import React from "react";

const Error = () => {
  return (
    <Flex justify="center" align="center">
      <Heading>Loading</Heading>;
    </Flex>
  );
};

export default Error;
