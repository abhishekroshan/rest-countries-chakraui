import { Flex, Input, Box, useColorMode } from "@chakra-ui/react";
import { Search2Icon } from "@chakra-ui/icons";
import React from "react";

const InputBox = ({ onSearch }) => {
  const { colorMode } = useColorMode();

  const valueChange = (e) => {
    onSearch(e.target.value);
    console.log(e.target.value);
  };

  return (
    <Flex
      gap="20px"
      align="center"
      px="10px"
      bg={colorMode === "light" ? "aliceblue" : "hsl(209, 23%, 22%)"}
      color={colorMode === "light" ? "black" : "white"}
    >
      <Search2Icon />

      <Input
        onChange={valueChange}
        placeholder="Search For A Country..."
        w="20rem"
        h="3rem"
        border="none"
        // size="lg"
        fontSize="1rem"
        bg={colorMode === "light" ? "white" : "hsl(209, 23%, 22%)"}
        color={colorMode === "light" ? "black" : "grey"}
      />
    </Flex>
  );
};

export default InputBox;
