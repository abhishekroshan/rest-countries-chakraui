import { Flex, Button } from "@chakra-ui/react";
import React from "react";

const AreaSort = ({ clicked }) => {
  return (
    <Flex justify="center" align="center" direction="column">
      Area
      <Button
        onClick={() => clicked("asc")}
        fontSize="10px"
        h="15px"
        border="1px solid black"
      >
        Asc
      </Button>
      <Button
        onClick={() => clicked("desc")}
        fontSize="10px"
        h="15px"
        border="1px solid black"
      >
        Desc
      </Button>
    </Flex>
  );
};

export default AreaSort;
