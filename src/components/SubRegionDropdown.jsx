import React from "react";
import { Select, useColorMode } from "@chakra-ui/react";

const SubRegionDropdown = ({ onSearch, allSubRegions }) => {
  const { colorMode } = useColorMode();

  const subRegionChange = (e) => {
    onSearch(e.target.value);
    console.log(e.target.value);
  };

  return (
    <Select
      onChange={subRegionChange}
      h="3rem"
      fontSize="1rem"
      bg={colorMode === "light" ? "white" : "hsl(209, 23%, 22%)"}
      color={colorMode === "light" ? "black" : "white"}
    >
      <option value="">Filter By SubRegion</option>
      {allSubRegions &&
        allSubRegions.map((subRegion) => (
          <option key={subRegion} value={subRegion}>
            {subRegion}
          </option>
        ))}
    </Select>
  );
};

export default SubRegionDropdown;
