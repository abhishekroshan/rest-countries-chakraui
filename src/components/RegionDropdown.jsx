import { Select, useColorMode } from "@chakra-ui/react";
import React from "react";

const RegionDropdown = ({ onSearch, allRegions }) => {
  const { colorMode } = useColorMode();
  const regionChange = (e) => {
    onSearch(e.target.value);
    console.log(e.target.value);
  };

  return (
    <Select
      onChange={regionChange}
      h="3rem"
      fontSize="1rem"
      bg={colorMode === "light" ? "white" : "hsl(209, 23%, 22%)"}
      color={colorMode === "light" ? "black" : "white"}
    >
      <option value="">Filter By Region</option>
      {allRegions &&
        allRegions.map((region) => (
          <option key={region} value={region}>
            {region}
          </option>
        ))}
    </Select>
  );
};

export default RegionDropdown;
