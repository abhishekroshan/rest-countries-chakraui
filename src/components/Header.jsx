import {
  Box,
  Flex,
  Text,
  useColorMode,
  useColorModeValue,
} from "@chakra-ui/react";
import { MoonIcon } from "@chakra-ui/icons";
import React, { useContext } from "react";

const Header = () => {
  const { colorMode, toggleColorMode } = useColorMode();

  //   const colors = useColorModeValue("black", "white");
  //   const bgColor = useColorModeValue("white", "black");

  return (
    <Flex
      px="60px"
      py="30px"
      justify="space-between"
      align="center"
      //   color={colors}
      //   bg={bgColor}
    >
      <Box as="div" fontSize="2rem" fontWeight="700">
        Where In The World
      </Box>
      <Box>
        <Flex
          gap="10px"
          align="center"
          cursor="pointer"
          onClick={toggleColorMode}
        >
          <MoonIcon />
          <Text as="div" fontSize="18px">
            Dark Mode
          </Text>
        </Flex>
      </Box>
    </Flex>
  );
};

export default Header;
