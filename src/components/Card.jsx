import { Text, Flex, Grid, Image, useColorMode, Box } from "@chakra-ui/react";
import React from "react";

const Card = ({
  countries,
  inputValue,
  regionValue,
  subRegionValue,
  populationSort,
  areaSort,
}) => {
  const { colorMode } = useColorMode();

  let filteredCountries = countries.filter((country) => {
    if (
      (!inputValue ||
        country.name.common
          .toLowerCase()
          .startsWith(inputValue.toLowerCase())) &&
      (!regionValue || regionValue === country.region) &&
      (!subRegionValue || subRegionValue === country.subregion)
    ) {
      return country;
    }
  });

  if (populationSort === "asc") {
    filteredCountries = [...filteredCountries].sort(
      (a, b) => a.population - b.population
    );
  } else if (populationSort === "desc") {
    filteredCountries = [...filteredCountries].sort(
      (a, b) => b.population - a.population
    );
  } else if (areaSort === "asc") {
    filteredCountries = [...filteredCountries].sort((a, b) => a.area - b.area);
  } else if (areaSort === "desc") {
    filteredCountries = [...filteredCountries].sort((a, b) => b.area - a.area);
  }

  return (
    <Grid
      templateColumns="repeat(4, 1fr)"
      gap="10px"
      p="35px"
      bg={colorMode === "light" ? "aliceblue" : "black"}
      color={colorMode === "light" ? "black" : "white"}
    >
      {filteredCountries &&
        filteredCountries.map((country, index) => (
          <Flex
            key={index}
            direction="column"
            bg="white"
            maxW="24rem"
            mx="25px"
            my="20px"
          >
            <Image src={country.flags.png} h="12rem" w="inherit" />
            <Box
              p="25px"
              bg={colorMode === "light" ? "whitesmoke" : "hsl(209, 23%, 22%)"}
              color={colorMode === "light" ? "black" : "white"}
            >
              <Text fontSize="1.4rem" fontWeight="700">
                {country.name.common}
              </Text>
              <Box py="15px">
                <Text as="div" fontWeight="500">
                  Population: {country.population}
                </Text>
                <Text as="div" fontWeight="500">
                  Region: {country.region}
                </Text>
                <Text as="div" fontWeight="500">
                  Capital: {country.capital}
                </Text>
              </Box>
            </Box>
          </Flex>
        ))}
    </Grid>
  );
};

export default Card;
