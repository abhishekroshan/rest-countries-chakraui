import { Flex, Heading } from "@chakra-ui/react";
import React from "react";

const Loading = () => {
  return (
    <Flex justify="center" align="center">
      <Heading>Loading...</Heading>;
    </Flex>
  );
};

export default Loading;
