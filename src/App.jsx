import { useEffect, useState } from "react";
import { Box, Flex, useColorMode } from "@chakra-ui/react";
import "./App.css";
import Header from "./components/Header";
import Card from "./components/Card";
import InputBox from "./components/InputBox";
import RegionDropdown from "./components/RegionDropdown";
import SubRegionDropdown from "./components/SubRegionDropdown";
import PopulationSort from "./components/PopulationSort";
import AreaSort from "./components/AreaSort";
import Error from "./components/Error";
import Loading from "./components/Loading";

function App() {
  const { colorMode } = useColorMode();

  const [allCountries, setAllCountries] = useState([]);
  const [inputValue, setInputValue] = useState("");
  const [regionAndSubRegionData, setRegionAndSubRegionData] = useState({});
  const [region, setRegion] = useState("");
  const [subRegion, setSubRegion] = useState("");
  const [population, setPopulation] = useState("");
  const [area, setArea] = useState("");
  const [checkError, setCheckError] = useState(false);
  const [checkLoading, setCheckLoading] = useState(true);

  const API_URL = "https://restcountries.com/v3.1/all";

  const fetchCountriesData = async () => {
    try {
      const res = await fetch(API_URL);

      const data = await res.json();

      return data;
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    const fetchData = async () => {
      try {
        const countriesData = await fetchCountriesData();
        setAllCountries(countriesData);
        setCheckLoading(false);

        const regionAndSubregions = countriesData.reduce(
          (accumulator, currentValue) => {
            const region = currentValue.region;
            const subRegion = currentValue.subregion;

            if (!accumulator[region]) {
              accumulator[region] = [];
            }

            if (!accumulator[region].includes(subRegion)) {
              accumulator[region].push(subRegion);
            }

            return accumulator;
          },
          {}
        );

        setRegionAndSubRegionData(regionAndSubregions);
      } catch (error) {
        console.log(error);
        setCheckError(true);
      }
    };

    fetchData();
  }, []);

  // console.log(regionAndSubRegionData);

  //======================================================================

  const handleValueChange = (value) => {
    setPopulation("");
    setInputValue(value);
  };

  const handleRegionChange = (value) => {
    setPopulation("");
    setSubRegion("");
    setRegion(value);
  };

  const handleSubRegionChange = (value) => {
    setPopulation("");
    setSubRegion(value);
  };

  //======================================================================

  const handlePopulationSort = (order) => {
    setPopulation(order);
    setArea("");
  };

  const handleAreaSort = (order) => {
    setPopulation("");
    setArea(order);
  };

  //======================================================================

  let content;

  if (checkLoading) {
    if (checkError) {
      content = <Error />;
    } else {
      content = <Loading />;
    }
  } else {
    content = (
      <Card
        countries={allCountries}
        inputValue={inputValue}
        regionValue={region}
        subRegionValue={subRegion}
        populationSort={population}
        areaSort={area}
      />
    );
  }

  //======================================================================

  return (
    <>
      <Header />
      <Box>
        <Flex
          align="center"
          py="20px"
          px="4rem"
          gap="160px"
          bg={colorMode === "light" ? "aliceblue" : "black"}
          color={colorMode === "light" ? "black" : "white"}
        >
          <InputBox onSearch={handleValueChange} />
          <RegionDropdown
            onSearch={handleRegionChange}
            allRegions={Object.keys(regionAndSubRegionData)}
          />
          <SubRegionDropdown
            onSearch={handleSubRegionChange}
            allSubRegions={regionAndSubRegionData[region]}
          />
          <PopulationSort clicked={handlePopulationSort} />
          <AreaSort clicked={handleAreaSort} />
        </Flex>
      </Box>
      {content}
    </>
  );
}

export default App;
